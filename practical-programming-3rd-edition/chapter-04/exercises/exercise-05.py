x = 3
y = 12.5

#a
print("The rabbit is ", x, ".", sep="")
#b
print("The rabbit is", x, "years old.")
#c
print(y, "is average.")
#d
print(y * 3)
#e
print(str(y), "* 3 is", str(y * 3) + ".")
