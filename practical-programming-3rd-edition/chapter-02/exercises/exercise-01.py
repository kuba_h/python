#a
print(9 - 3)
#b
print(8 * 2.5)
#c
print(9 / 2)
#d
print(9 / -2)
#e
print(9 // -2)
#f
print(9 % 2)
#g
print(9.0 % 2)
#h
print(9 % 2.0)
#i
print(9 % -2)
#j
print(-9 % 2)
#k
print(9 / -2.0)
#l
print(4 + 3 * 5)
#m
print((4 + 3) * 5)
