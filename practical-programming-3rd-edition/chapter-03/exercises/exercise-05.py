def distance_in_miles(number: float) -> float:
    """
    Function returns distance in miles for a given distance in kilometers.

    >>> distance_in_miles(1)
    0.625
    >>> distance_in_miles(4)
    2.5
    >>> distance_in_miles(7)
    4.375
    """

    return number / 1.6

print(distance_in_miles(1))
print(distance_in_miles(4))
print(distance_in_miles(7))
