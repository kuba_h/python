def grades_average_3(grade1: int, grade2: int, grade3: int) -> float:
    """
    Precondition: each grade must be within 0 - 100 range inclusive.
    Function returns average of three given grades.

    >>> grades_average(10, 10, 10)
    10.0
    >>> grades_average(40, 40, 100)
    60.0
    >>> grades_average(100, 100, 10)
    70.0
    """

    return (grade1 + grade2 + grade3) / 3

def grades_average_4(grade1: int, grade2: int, grade3: int, grade4: int) -> float:
    """
    Precondition: each grade must be within 0 - 100 range inclusive.
    Function returns average of top three given grades.

    >>> grades_average(10, 10, 10, 0)
    10.0
    """

    return max(
            grades_average_3(grade1, grade2, grade3),
            grades_average_3(grade1, grade2, grade4),
            grades_average_3(grade1, grade3, grade4),
            grades_average_3(grade2, grade3, grade4)
            )

print(grades_average_4(10, 10, 10, 0))
print(grades_average_4(100, 75, 80, 90))
print(grades_average_4(33, 50, 100, 10))
