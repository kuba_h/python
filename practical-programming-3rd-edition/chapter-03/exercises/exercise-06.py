def grades_average(grade1: int, grade2: int, grade3: int) -> float:
    """
    Precondition: each grade must be within 0 - 100 range inclusive.
    Function returns average of three given grades.

    >>> grades_average(10, 10, 10)
    10.0
    >>> grades_average(40, 40, 100)
    60.0
    >>> grades_average(100, 100, 10)
    70.0
    """

    return (grade1 + grade2 + grade3) / 3

print(grades_average(10, 10, 10))
print(grades_average(40, 40, 100))
print(grades_average(100, 100, 10))
