def square(num):
    """
    (number) -> number

    Return the square of num.

    >>> square(3)
    9
    """

    return num * num

print(square(3))
print(square(4))
print(square(10))
