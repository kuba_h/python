def abs_diff(number1: float, number2: float) -> float:
    """
    Function returns the absolute value of the difference of the two numbers.

    >>> abs_diff(1, 2)
    1
    >>> abs_diff(4, -4)
    8
    >>> abs_diff(7, 2)
    5
    """

    return abs(number1 - number2)

print(abs_diff(1, 2))
print(abs_diff(4, -4))
print(abs_diff(7, 2))
