def tripled(number: float) -> float:
    """
    For a given number function returns that number tripled.

    >>> tripled(2)
    8
    >>> tripled(1.2)
    1.728
    >>> tripled(-4)
    -64
    """

    return number * 3

print(tripled(2))
print(tripled(1.2))
print(tripled(-4))
